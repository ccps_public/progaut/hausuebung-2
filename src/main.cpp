#include <iostream>

#include "LinkedList.hpp"

int main() {
    ListItem* a = new ListItem{"a"};
    ListItem* b = new ListItem{"b"};
    ListItem* c = new ListItem{"c"};

    b->insertAfter(a);  // a <-> b
    c->insertAfter(b);  // a <-> b <-> c

    a->remove();        // b <-> c
    a->insertAfter(b);  // b <-> a <-> c

    ListItem* p = b;  // get pointer to first element of list

    // traverse list and print content
    while (true) {
        std::cout << p->getContent();
        p = p->getNext();

        if (p == nullptr) {
            break;
        }
        std::cout << " <-> ";
    }

    delete c;
    delete b;
    delete a;

    std::cout << "\n\n\n";

    LinkedList list{};

    list.push_back("a");
    list.push_back("b");
    list.push_back("c");

    list.pop_front();

    list.insert("a", 1);

    while (!list.empty()) {
        std::cout << list.pop_front() << " ";
    }

    return 0;
}