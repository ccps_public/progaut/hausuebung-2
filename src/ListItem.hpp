#ifndef PROGAUT_HAUSUEBUNG2_LISTITEM_HPP
#define PROGAUT_HAUSUEBUNG2_LISTITEM_HPP

#include <string>

// This _DUMMY_STR_LIST_ITEM is only a placeholder that is used
// for the dummy functions that return a const reference to a string.
// It should be removed for the final solution.
static const std::string _DUMMY_STR_LIST_ITEM{""};

class ListItem {
   public:
    ListItem(const std::string content) {}

    ~ListItem() {}

    ListItem(const ListItem&) = delete;
    ListItem& operator=(const ListItem&) = delete;
    ListItem(ListItem&&) = delete;
    ListItem& operator=(ListItem&&) = delete;

    ListItem* getNext() const { return nullptr; }
    ListItem* getPrevious() const { return nullptr; }

    const std::string& getContent() const { return _DUMMY_STR_LIST_ITEM; }

    void insertBefore(ListItem* item) {}

    void insertAfter(ListItem* item) {}

    void remove() {}
};

#endif  // PROGAUT_HAUSUEBUNG2_LISTITEM_HPP