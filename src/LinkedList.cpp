#include "LinkedList.hpp"

#include <stdexcept>

// This _DUMMY_STR_LINKED_LIST is only a placeholder that is used
// for the dummy functions that return a const reference to a string.
// It should be removed for the final solution.
static const std::string _DUMMY_STR_LINKED_LIST{""};

std::string LinkedList::remove(size_t position) { return _DUMMY_STR_LINKED_LIST; }

std::string LinkedList::pop_front() { return _DUMMY_STR_LINKED_LIST; }

std::string LinkedList::pop_back() { return _DUMMY_STR_LINKED_LIST; }

const std::string& LinkedList::get(size_t position) const { return _DUMMY_STR_LINKED_LIST; }

const std::string& LinkedList::back() const { return _DUMMY_STR_LINKED_LIST; }

const std::string& LinkedList::front() const { return _DUMMY_STR_LINKED_LIST; }

void LinkedList::insert(const std::string element, size_t position) {}

void LinkedList::push_front(const std::string element) {}

void LinkedList::push_back(const std::string element) {}

void LinkedList::prepend(LinkedList list) {}

void LinkedList::append(LinkedList list) {}

void LinkedList::insert_sorted(const std::string element) {}
