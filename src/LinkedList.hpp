#ifndef PROGAUT_HAUSUEBUNG2_LINKEDLIST_HPP
#define PROGAUT_HAUSUEBUNG2_LINKEDLIST_HPP

#include <cstddef>
#include <string>

#include "ListItem.hpp"

class LinkedList {
   public:
    LinkedList() {}

    ~LinkedList() {}

    LinkedList(const LinkedList& other) {}

    LinkedList& operator=(const LinkedList& other) { return *this; }

    bool empty() const { return false; }
    size_t size() const { return 0; }

    const std::string& front() const;
    const std::string& back() const;
    const std::string& get(size_t position) const;

    void push_front(const std::string element);
    void push_back(const std::string element);
    void insert(const std::string element, size_t position);

    std::string pop_front();
    std::string pop_back();
    std::string remove(size_t position);

    void prepend(LinkedList list);
    void append(LinkedList list);

    void insert_sorted(const std::string element);
};

#endif  // PROGAUT_HAUSUEBUNG2_LINKEDLIST_HPP