#include "ListItem.hpp"
#include <gtest/gtest.h>

TEST(ListItemTest, ConstructorAndGetter) {
    auto* a = new ListItem{"3"};

    EXPECT_EQ(a->getPrevious(), nullptr);
    EXPECT_EQ(a->getContent(), "3");

    delete a;
}

TEST(ListItemTest, InsertBefore) {
    ListItem a{"A"};
    ListItem b{"B"};

    a.insertBefore(&b);

    EXPECT_EQ(a.getPrevious(), nullptr);
    EXPECT_EQ(a.getNext(), &b);
    EXPECT_EQ(b.getPrevious(), &a);
    EXPECT_EQ(b.getNext(), nullptr);

    EXPECT_EQ(a.getContent(), "A");
    EXPECT_EQ(b.getContent(), "B");
}

TEST(ListItemTest, InsertBefore3AlwaysFront) {
    ListItem a{"A"};
    ListItem b{"B"};
    ListItem c{"C"};

    b.insertBefore(&c);
    a.insertBefore(&b);

    EXPECT_EQ(a.getPrevious(), nullptr);
    EXPECT_EQ(a.getNext(), &b);
    EXPECT_EQ(b.getPrevious(), &a);
    EXPECT_EQ(b.getNext(), &c);
    EXPECT_EQ(c.getPrevious(), &b);
    EXPECT_EQ(c.getNext(), nullptr);

    EXPECT_EQ(a.getContent(), "A");
    EXPECT_EQ(b.getContent(), "B");
    EXPECT_EQ(c.getContent(), "C");
}

TEST(ListItemTest, InsertBefore3IntoMiddle) {
    ListItem a{"A"};
    ListItem b{"B"};
    ListItem c{"C"};

    b.insertBefore(&c);
    a.insertBefore(&c);

    EXPECT_EQ(b.getPrevious(), nullptr);
    EXPECT_EQ(b.getNext(), &a);
    EXPECT_EQ(a.getPrevious(), &b);
    EXPECT_EQ(a.getNext(), &c);
    EXPECT_EQ(c.getPrevious(), &a);
    EXPECT_EQ(c.getNext(), nullptr);

    EXPECT_EQ(a.getContent(), "A");
    EXPECT_EQ(b.getContent(), "B");
    EXPECT_EQ(c.getContent(), "C");
}

TEST(ListItemTest, Remove) {
    ListItem a{"A"};
    ListItem b{"B"};
    ListItem c{"C"};

    b.insertBefore(&c);
    a.insertBefore(&b);

    b.remove();

    EXPECT_EQ(a.getPrevious(), nullptr);
    EXPECT_EQ(a.getNext(), &c);
    EXPECT_EQ(c.getPrevious(), &a);
    EXPECT_EQ(c.getNext(), nullptr);

    EXPECT_EQ(b.getPrevious(), nullptr);
    EXPECT_EQ(b.getNext(), nullptr);

    EXPECT_EQ(a.getContent(), "A");
    EXPECT_EQ(b.getContent(), "B");
    EXPECT_EQ(c.getContent(), "C");
}

TEST(ListItemTest, Destructor) {
    ListItem a{"A"};
    ListItem* b = new ListItem{"B"};
    ListItem c{"C"};

    b->insertBefore(&c);
    a.insertBefore(b);

    delete b;

    EXPECT_EQ(a.getPrevious(), nullptr);
    EXPECT_EQ(a.getNext(), &c);
    EXPECT_EQ(c.getPrevious(), &a);
    EXPECT_EQ(c.getNext(), nullptr);

    EXPECT_EQ(a.getContent(), "A");
    EXPECT_EQ(c.getContent(), "C");
}
