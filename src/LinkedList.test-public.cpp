#include "LinkedList.hpp"
#include <gtest/gtest.h>

TEST(LinkedListTest, ConstructorAndFrontOperations) {
    LinkedList list{};

    list.push_front("a");
    EXPECT_EQ(list.front(), "a");
    EXPECT_EQ(list.pop_front(), "a");
}

TEST(LinkedListTest, ConstructorAndMultipleFrontOperations) {
    LinkedList list{};

    list.push_front("a");
    list.push_front("b");
    EXPECT_EQ(list.front(), "b");
    EXPECT_EQ(list.pop_front(), "b");
    EXPECT_EQ(list.pop_front(), "a");
}

TEST(LinkedListTest, Size) {
    LinkedList list{};

    EXPECT_EQ(list.size(), 0);

    list.push_front("a");
    EXPECT_EQ(list.size(), 1);

    list.push_front("b");
    EXPECT_EQ(list.size(), 2);

    EXPECT_EQ(list.pop_front(), "b");
    EXPECT_EQ(list.size(), 1);

    EXPECT_EQ(list.pop_front(), "a");
    EXPECT_EQ(list.size(), 0);
}

TEST(LinkedListTest, Empty) {
    LinkedList list{};

    EXPECT_TRUE(list.empty());

    list.push_front("a");
    EXPECT_FALSE(list.empty());

    list.push_front("b");
    EXPECT_FALSE(list.empty());

    list.pop_front();
    EXPECT_FALSE(list.empty());

    list.pop_front();
    EXPECT_TRUE(list.empty());
}

TEST(LinkedListTest, BackOperations) {
    LinkedList list{};

    list.push_front("a");
    list.push_back("b");
    EXPECT_EQ(list.front(), "a");
    EXPECT_EQ(list.back(), "b");
    EXPECT_EQ(list.pop_back(), "b");
    EXPECT_EQ(list.pop_back(), "a");
    EXPECT_TRUE(list.empty());
}

TEST(LinkedListTest, IndexedOperations) {
    LinkedList list{};

    list.push_front("b");
    list.push_front("a");
    EXPECT_EQ(list.get(0), "a");
    EXPECT_EQ(list.get(1), "b");
    list.insert("c", 1);

    EXPECT_EQ(list.size(), 3);
    EXPECT_EQ(list.get(0), "a");
    EXPECT_EQ(list.get(1), "c");
    EXPECT_EQ(list.get(2), "b");

    list.remove(0);
    EXPECT_EQ(list.size(), 2);
    EXPECT_EQ(list.front(), "c");
}

TEST(LinkedListTest, CopyConstructor) {
    LinkedList list{};

    list.push_front("c");
    list.push_front("b");
    list.push_front("a");

    LinkedList list2{list};

    list.pop_front();

    EXPECT_EQ(list2.pop_front(), "a");
    EXPECT_EQ(list2.pop_front(), "b");
    EXPECT_EQ(list2.pop_front(), "c");

    EXPECT_TRUE(list2.empty());

    EXPECT_EQ(list.size(), 2);
    EXPECT_EQ(list.pop_front(), "b");
    EXPECT_EQ(list.pop_front(), "c");
    EXPECT_TRUE(list.empty());
}

TEST(LinkedListTest, CopyAssignment) {
    LinkedList list{};

    list.push_front("c");
    list.push_front("b");
    list.push_front("a");

    LinkedList list2{};
    list2.push_front("d");

    list2 = list;

    list.pop_front();

    EXPECT_EQ(list2.pop_front(), "a");
    EXPECT_EQ(list2.pop_front(), "b");
    EXPECT_EQ(list2.pop_front(), "c");

    EXPECT_TRUE(list2.empty());

    EXPECT_EQ(list.size(), 2);
    EXPECT_EQ(list.pop_front(), "b");
    EXPECT_EQ(list.pop_front(), "c");
    EXPECT_TRUE(list.empty());
}

TEST(LinkedListTest, Append) {
    LinkedList list{};

    list.push_front("c");
    list.push_front("b");
    list.push_front("a");

    LinkedList list2;
    list2.push_front("e");
    list2.push_front("d");

    list.append(list2);

    EXPECT_EQ(list.pop_front(), "a");
    EXPECT_EQ(list.pop_front(), "b");
    EXPECT_EQ(list.pop_front(), "c");
    EXPECT_EQ(list.pop_front(), "d");
    EXPECT_EQ(list.pop_front(), "e");

    EXPECT_TRUE(list.empty());
}

TEST(LinkedListTest, InsertSorted) {
    LinkedList list{};

    list.push_front("g");

    list.insert_sorted("a");
    list.insert_sorted("i");
    list.insert_sorted("h");

    EXPECT_EQ(list.size(), 4);
    EXPECT_EQ(list.pop_front(), "a");
    EXPECT_EQ(list.pop_front(), "g");
    EXPECT_EQ(list.pop_front(), "h");
    EXPECT_EQ(list.pop_front(), "i");

    EXPECT_TRUE(list.empty());
}
